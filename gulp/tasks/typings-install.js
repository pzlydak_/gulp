var console = require('../helpers/console'),
    Q = require('q'),
    exec = require('child_process').exec;
    
module.exports = function (config) {

    console.info("typings install");
    console.info("typings install: installing typescript definition files");

    var deferred = Q.defer();

    exec('typings install', function (err, stdout, stderr) {
        console.error(stdout);
        console.error(stderr);
        deferred.resolve();
        console.log("typings install: done");
    });

    return deferred.promise;

}