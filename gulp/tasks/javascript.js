var Q = require('q'),
    buffer = require('vinyl-buffer'),
    browserify = require('browserify'),
    sourcemaps = require('gulp-sourcemaps'),
    source = require('vinyl-source-stream');

// config = {
//     src: {
//         main: config.src.path + '/js/index.js'
//     },
//     dist: {
//         filename: 'index.js',
//         path: config.dist.path + '/assets/js'
//     },
//     sourcemaps: true
// };

module.exports = function(config) {
    
    return function () {
        
        console.info("compile javascript");
        console.info("javascript: main: " + config.src.main);
        console.info("javascript: dist file: " + config.dist.filename);
        console.info("javascript: dist path: " + config.dist.path);
        console.info("javascript: sourcemaps: " + config.sourcemaps);
        
        var deferred = Q.defer();

        var stream = browserify()
        .add(config.src.main)
        .bundle()
        .on('error', function (error) { 
            console.error(error);
        })
        .pipe(source(config.dist.filename))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: config.sourcemaps}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(config.dist.path));
        
        stream.on("finish", function () {
            deferred.resolve();
        });

        return deferred.promise;
    }
    
}