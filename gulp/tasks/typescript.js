var Q = require('q'),
    buffer = require('vinyl-buffer'),
    tsify = require('tsify'),
    browserify = require('browserify'),
    sourcemaps = require('gulp-sourcemaps'),
    source = require('vinyl-source-stream');

// config = {
//     src: {
//         main: config.src.path + '/ts/index.ts'
//     },
//     dist: {
//         filename: 'index.js',
//         path: config.dist.path + '/assets/js'
//     },
//     sourcemaps: true
// };

module.exports = function(config) {
    
    return function () {
        
        console.info("compile typescript");
        console.info("typescript: main: " + config.src.main);
        console.info("typescript: dist file: " + config.dist.filename);
        console.info("typescript: dist path: " + config.dist.path);
        console.info("typescript: sourcemaps: " + config.sourcemaps);
        
        var deferred = Q.defer();

        var stream = browserify()
        .add(config.src.main)
        .plugin(tsify)
        .bundle()
        .on('error', function (error) { 
            console.error(error);
        })
        .pipe(source(config.dist.filename))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: config.sourcemaps}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(config.dist.path));
        
        stream.on("finish", function () {
            deferred.resolve();
        });

        return deferred.promise;
    }
    
}