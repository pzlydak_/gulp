module.exports = function (config) {
        
    var deferred = Q.defer();
    
    // Setup a live reload server
    var embedlr = require('gulp-embedlr'),
        refresh = require('gulp-livereload'),
        lrserver = require('tiny-lr')(),
        express = require('express'),
        livereload = require('connect-livereload'),
        livereloadport = config.livereloadport,
        serverport = config.serverport;

    console.info("starting localhost webserver on port: " + serverport);

    // Set up an express server (but not starting it yet)
    var server = express();
    // Add live reload
    server.use(livereload({port: livereloadport}));
    // Use our 'dist' folder as rootfolder
    server.use(express.static(config.dist.path));
    // Start webserver
    server.listen(serverport);
    // Start live reload
    lrserver.listen(livereloadport);
    
    deferred.resolve();
    
    return deferred.promise;

}