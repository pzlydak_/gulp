var async = require('async'),
    gulp = require('gulp'),
    Q = require('q'),
    console = require('../helpers/console');

module.exports = function (config) {
	
    return function () {
        var deferred = Q.defer();
    
        async.each(config.files, function (item, callback) {
        
            copy(item.src, item.dist, function (err, res) {
                callback(err);
            });
            
        }, function (err, res){
            
            if (err) {
                deferred.reject(err);
                return;
            }
            
            deferred.resolve();
            
        });
        
        return deferred.promise;
    }
    
}

function copy(src, dist, callback) {
    
    console.log("copy");
    console.info("copy: source: " + src);
    console.info("copy: dist: " + dist);
    
    var stream = gulp.src(src)
    .pipe(gulp.dest(dist));
    
    stream.on("finish", function () {
        callback();
    }).on("error", function (error) {
       callback(error);
    });
    
}