var async = require('async'),
    Q = require('q'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat');

// var sassConfig = {
//     files: [
//         /path/to/sass/**/*.scss
//     ],
//     dist: {
//         filename: 'filename.css',
//         path: /path/to/dist/css
//     }
// };

module.exports = function (config) {
    
    return function () {
        
        console.log("compile sass");
        console.info("sass: source files: " + config.files);
        console.info("sass: dist filename: " + config.dist.filename)
        console.info("sass: dist path: " + config.dist.path)
        
        var deferred = Q.defer();
        
        var stream = gulp.src(config.files)
        .pipe(sass().on("error", function (err) {
            console.error(err);
        }))
        .pipe(autoprefixer("last 2 versions", "> 1%", "ie 8"))
        .pipe(concat(config.dist.filename))
        .pipe(gulp.dest(config.dist.path));
        
        stream.on("finish", function () {
            deferred.resolve();
        }).on("error", function (err) {
            console.error(err);
            deferred.reject(err);
        });

        return deferred.promise;
        
    }

}