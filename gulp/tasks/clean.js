var async = require('async')
    fs = require('fs')
    gulp = require('gulp')
    Q = require('q'),
    gulpClean = require('gulp-clean'),
    console = require('../helpers/console')

// config = {
//     files: [
//         '/path/to/directory'
//     ]
// };

module.exports = function (config) {
	
    return function () {
        
        console.log("clean");
        
        var deferred = Q.defer();
        
        async.each(config.files, function (item, callback) {
            
            clean(item, function (err, res) {
                callback(err);
            });
            
        }, function (err, res){
            
            if (err) {
                deferred.reject(err);
                return;
            }
            
            deferred.resolve();
            
        });
        
        return deferred.promise;
    }
    
}

function clean(path, callback) {
    
    console.info("clean: path: " + path);
    
    fs.stat(path, function (err, res) {
        if (err)
            return callback(null);

        gulp.src(path)
        .pipe(gulpClean({
            read: false,
            force: false
        }))
        .on("finish", function () {
            callback(null);
        }).on("error", function (error) {
            callback(null);
        });
    });
    
}