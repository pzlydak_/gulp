var console = require('../helpers/console'),
    watch = require('gulp-watch');

// config = {
// 	items: [
// 		{
// 			files: [
// 				'/path/to/typescript/**/*.ts'
// 			],
// 			task: typescript,
// 			config: typescriptConfig
// 		},
// 		{
// 			files: [
// 				'/path/to/sass/**/*.scss',
// 				'!/path/to/sass/_*.scss'
// 			],
// 			task: sass,
// 			config: sassConfig
// 		},
// 		{
// 			files: [
// 				'/path/to/assets/**/*'
// 			],
// 			task: copy,
// 			config: copyConfig
// 		}
// 	]
// };

module.exports = function (config) {
    
    return function () {
        
        console.log("watch");
        
        var watchItems = config.items;
        
        for (var i = 0; i < watchItems.length; i++) {
            
            (function (i) {
                
                var watchItem = watchItems[i];
                
                console.info("watch: watching: " + watchItem.files);
                
                watch(watchItem.files, function () {
                    console.log("change!");
                    watchItem.task(watchItem.config)();
                }).on("error", function (err){
                    console.error(err);
                });
                
            })(i);
        }
        
    }
    
}