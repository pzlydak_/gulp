var console = require('../helpers/console'),
    Q = require('q'),
    exec = require('child_process').exec;

module.exports = function (config) {

    console.log("npm install");
    console.info("npm install: installing javascript dependencies... this may take a moment");

    var deferred = Q.defer();

    exec('npm install', function (err, stdout, stderr) {
        console.error(err);
        console.error(stdout);
        console.error(stderr);
        deferred.resolve();
        console.log("npm install: done");
    });

    return deferred.promise;

}