var console = require('../helpers/console'),
    Q = require('q'),
    async = require('async');

// config = {
// 	packages: [
// 		'npm-install',
// 		'typings-install'
// 	]
// };

module.exports = function (config) {
    
    return function () {
        
        console.log("install");
        
        var deferred = Q.defer();
        var packages = [];
        
        for (var i = 0; i < config.packages.length; i++) {
            packages.push(require('./' + config.packages[i]));
        }
        
        async.eachSeries(packages, function(package, callback) {
            
            package(config)
            .then(function(response) {
                callback(null);
            }, function (error) {
                callback(error);
            });
            
        }, function (err, res) {
            deferred.resolve();
        });
        
        return deferred.promise;
        
    }
    
}