module.exports = {
    getTimestamp: function() {
        var d = new Date();
        var hh = this.addZero(d.getHours());
        var mm = this.addZero(d.getMinutes());
        var ss = this.addZero(d.getSeconds());
        var timestamp = hh + ":" + mm + ":" + ss;
        return timestamp;
    },
    addZero: function(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
}