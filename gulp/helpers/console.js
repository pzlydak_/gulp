// Override the console.log
var log = console.log;
var info = console.info;
var error = console.error;
var Utils = require('./utils');
var chalk = require('chalk');

// This console.log will output a timestamp with the message
console.log = function(msg){
	var timestamp = Utils.getTimestamp();
	log(chalk.green("[" + timestamp + "] " + msg));
}

console.info = function(msg){
	var timestamp = Utils.getTimestamp();
	info(chalk.yellow("[" + timestamp + "] " + msg));
}

console.error = function(msg){
	var timestamp = Utils.getTimestamp();
	error(chalk.red("[" + timestamp + "] " + msg));
}

module.exports = console;