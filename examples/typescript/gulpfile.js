var gulp = require('gulp'),
	console = require('../../gulp/helpers/console'),
	copy = require('../../gulp/tasks/copy'),
	clean = require('../../gulp/tasks/clean'),
	install = require('../../gulp/tasks/install'),
	typescript = require('../../gulp/tasks/typescript'),
	sass = require('../../gulp/tasks/sass'),
	watch = require('../../gulp/tasks/watch'),
	javascript = require('../../gulp/tasks/javascript');

// General config
var config = {
    root: __dirname,
	src: {
		path: './src',
		ts: {
			main: './src/ts/index.ts'
		}
	},
	dist: {
		path: './dist',
		js: {
			fileName: 'index.js'
		}
	},
	server: {
		startLocalhostServer: false,
		livereloadport: 35729,
		serverport: 5000
	}
};

// Copy config
var copyConfig = {
	files: [
		{
        	src: config.src.path + '/assets/**/*',
        	dist: config.dist.path + '/assets'
    	}
	]
};

// Clean config
var cleanConfig = {
	files: [
		config.dist.path
	]
};

// Sass config
var sassConfig = {
	files: [
		config.src.path + '/assets/**/*.scss',
	],
	dist: {
		filename: 'all.css',
		path: config.dist.path + '/assets/css'
	}
};

// Typescript config
var typescriptConfig = {
	src: {
		main: config.src.path + '/ts/index.ts'
	},
	dist: {
		filename: 'index.js',
		path: config.dist.path + '/assets/js'
	},
	sourcemaps: true
};

// Install config
var installConfig = {
	packages: [
		'npm-install',
		'typings-install'
	]
};

// Watch config
var watchConfig = {
	items: [
		{
			files: [
				config.src.path + '/**/*.ts'
			],
			task: typescript,
			config: typescriptConfig
		},
		{
			files: [
				config.src.path + '/**/*.scss',
				'!' + config.src.path + '/sass/_*.scss'
			],
			task: sass,
			config: sassConfig
		},
		{
			files: [
				config.src.path + '/assets/**/*'
			],
			task: copy,
			config: copyConfig
		}
	]
};

gulp.task("copy", copy(copyConfig));
gulp.task("clean", clean(cleanConfig));
gulp.task("sass", sass(sassConfig));
gulp.task("typescript", typescript(typescriptConfig));
gulp.task("install", install(installConfig));
gulp.task("watch", watch(watchConfig));

// Dev task
gulp.task('dev', gulp.series('clean', gulp.parallel('copy', 'typescript', 'sass'), 'watch'));